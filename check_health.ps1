param([Parameter(Mandatory=$True)][string]$healthEndpoint, [Parameter(Mandatory=$True)][string]$deploymentId)

$timeout = New-Timespan -Minutes 1
$sw = [diagnostics.stopwatch]::StartNew()

While ($sw.elapsed -lt $timeout) 
{
    $result = $null

    Try
    {
        $result = Invoke-RestMethod -Uri "$healthEndpoint/$deploymentId" -TimeoutSec 2
    }
    Catch {}

    if ($result -ne $null -and $result.status -eq "Ok") {
       Write-Host "Deployment $deploymentid is healthy"
       return
    }

    $remaining = ($timeout - $sw.Elapsed).Seconds
    
    Write-Host "$remaining seconds remaining - polling again for healthy deployment..."
    Start-Sleep -Seconds 5
}

Throw "Deployment $deploymentId is not healthy"