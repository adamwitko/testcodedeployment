param([Parameter(Mandatory=$True)]$path, [Parameter(Mandatory=$True)][string]$deploymentId)

Write-Output "Creating Deployfile for deployment $deploymentId"

New-Item -Path "$path\Deployfile" -Force -ItemType file -Value $deploymentId